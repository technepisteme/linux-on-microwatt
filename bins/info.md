# Bitstream information

- FPGA: *Arty A7-100T* or *Nexys Video*
- Toolchain: *Xilinx Vivado 2023.1*

Provided kernels:

- 5.7 (compiled by joel, file from ftp.libre-soc.org)
    - Might need seperate sdram init, **wasn't able to get working yet**.
- 6.5.0 (compiled by myself on Debian 12 Bookworm)
    - Commit: 3f86ed6ec0b390c033eae7f9c487a3fea268e027
    - Date: Mon Sep 4 15:38:24 2023 -0700
    - uname -a gives

    Linux microwatt 6.5.0-11704-g3f86ed6ec0b3 #1 Tue Sep  5 16:06:48 BST 2023 ppc64le GNU/Linux

To program (assumes microwatt repo at the same directory level as linux-on-microwatt):

    cd /PATH/TO/MICROWATT
    ./openocd/flash-arty -f a100 ../linux-on-microwatt/bins/arty_a7-100/microwatt_0.bit
    ./openocd/flash-arty -f a100 dtbImage.microwatt.elf -t bin -a 0x400000

For Nexys Video (need to find out address...):

    ./openocd/flash-arty -f a100 ../linux-on-microwatt/bins/nexys-video/microwatt_0.bit
    ./openocd/flash-arty -f a100 dtbImage.microwatt.elf -t bin -a `ADDRESS`
