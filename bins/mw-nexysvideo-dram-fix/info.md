# Bitstream information

- FPGA: *Nexys Video*
- Toolchain: *Xilinx Vivado 2023.1*
- Microwatt with DRAM fix: Commit 49d0ae59e3cb92413c0d2043905d3c54ed4dc0da

Due to possible litedram changes, using fork of microwatt which uses an older version of generate
dram controller. See the following links for more info:

- <https://github.com/antonblanchard/microwatt/issues/363>
- <https://github.com/shingarov/microwatt/tree/fix-dram>

Two bitstreams are provided:

- The usual hello world (only bitstream required)
- Bitstream for Linux kernel (kernel flashed separately)

Provided kernels:

- 6.5.0 (compiled by myself on Debian 12 Bookworm)
    - Commit: 3f86ed6ec0b390c033eae7f9c487a3fea268e027
    - Date: Mon Sep 4 15:38:24 2023 -0700
    - uname -a gives

    Linux microwatt 6.5.0-11704-g3f86ed6ec0b3 #1 Tue Sep  5 16:06:48 BST 2023 ppc64le GNU/Linux

To program (assumes microwatt repo at the same directory level as linux-on-microwatt):

    cd /PATH/TO/MICROWATT
    ./openocd/flash-arty -f a100 ../linux-on-microwatt/bins/mw-nexysvideo-dram-fix/2023-09-09_linux/microwatt_0.bit
    ./openocd/flash-arty -f a100 dtbImage.microwatt.elf -t bin -a `ADDRESS`
