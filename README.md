# Rough guide for producing a Microwatt bitstream using Xilinx Vivado,
# and compiling Linux kernel

You can skip Fusesoc and synthesis, and just focus on the kernel.

Tutorials used for this one:

- <https://shenki.github.io/boot-linux-on-microwatt/>
- <https://github.com/antonblanchard/microwatt>
- <https://codeconstruct.com.au/docs/microwatt-orangecrab/>

All files are under the `microwatt_work` directory:

    microwatt_work
          |
          |-------> buildroot
          |-------> linux
          |-------> microwatt
          |-------> mw_latest_fusesoc


## Venv - Only relevant for Debian 12 (can't use pip without venv by default)

Create Python3 virtual environment

    python3 -m venv .venv
    mkdir -p ~/microwatt_work
    cd ~/microwatt_work
    source .venv/bin/activate
    python3 -m pip install fusesoc

Fusesoc must be used within this virtual environment.

## For running the hello world bitstream

Compile hello_world

    sudo apt install gcc-12-powerpc64-linux-gnu gcc-12-powerpc64le-linux-gnu

At least on debian 12, no alias is created to gcc, so either have to add '-12' to Makefile

    sudo ln -sf /usr/bin/powerpc64le-linux-gnu-gcc-12 /usr/bin/powerpc64le-linux-gnu-gcc-12

Get Microwatt repo

    cd ~/microwatt_work
    git clone https://github.com/antonblanchard/microwatt.git
    cd ~/microwatt_work/microwatt/hello_world
    # Add "-12" to Makefile before running 'make'
    make

## Fusesoc setup - 'FPGA pkg manager'

    cd ~/microwatt_work
    mkdir mw_latest_fusesoc
    cd mw_latest_fusesoc/
    fusesoc library add fusesoc-cores https://github.com/fusesoc/fusesoc-cores
    fusesoc fetch uart16550
    fusesoc library add microwatt ../microwatt

To synthesise the microwatt bitstream with hello world:

    source /opt/Xilinx/Vivado/2023.1/settings64.sh # Need to source Vivado env's before starting
    fusesoc run --target=arty_a7-100 microwatt --memory_size=16384 --ram_init_file=../microwatt/hello_world/hello_world.hex


## Compiling Linux kernel and buildroot setup

### buildroot

    cd ~/microwatt_work
    git clone -b microwatt https://github.com/shenki/buildroot
    cd buildroot
    make ppc64le_microwatt_defconfig
    make

The output is under `output/images/rootfs.cpio`.

### Kernel

I have not made any changes from the default, there may be better parameters, out of scope for my work right now.

    cd ~/microwatt_work
    git clone https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
    cd linux
    make ARCH=powerpc microwatt_defconfig
    make ARCH=powerpc CROSS_COMPILE=powerpc64le-linux-gnu- \
      CONFIG_INITRAMFS_SOURCE=/buildroot/output/images/rootfs.cpio -j`nproc`

    (Stack Protector buffer overflow detection (STACKPROTECTOR) [Y/n/?] (NEW) Y)
    (Strong Stack Protector (STACKPROTECTOR_STRONG) [Y/n/?] (NEW) Y)

The output is under `arch/powerpc/boot/dtbImage.microwatt.elf`.

To synthesise microwatt bitstream for Linux:

    fusesoc run --build --target=arty_a7-100 microwatt --no_bram --memory_size=0
    fusesoc run --build --target=nexys_video microwatt --no_bram --memory_size=0

The output is under build/microwatt_0/arty_a7-100-vivado/microwatt_0.bit.

## Programming the SPI flash

*This operation will overwrite the contents of your flash.*

You need to install the openocd tool, and use a wrapper program from the microwatt repo.

    sudo apt install openocd

For the Arty A7 A100, set `[FLASH_ADDRESS]` to *`0x400000`* and pass *`-f a100`*.

**NOTE**: I don't know the correct address for the Nexys Video. This something you'll need to find on your own.

    cd ~/microwatt_work/mw_latest_fusesoc
    ../microwatt/openocd/flash-arty -f a100 build/microwatt_0/arty_a7-100-vivado/microwatt_0.bit
    ../microwatt/openocd/flash-arty -f a100 dtbImage.microwatt.elf -t bin -a `[FLASH_ADDRESS]`

Loading the kernel image:

    ../microwatt/openocd/flash-arty -f a100 build/microwatt_0/arty_a7-100-vivado/microwatt_0.bit
    ../microwatt/openocd/flash-arty -f a100 ../linux/arch/powerpc/boot/dtbImage.microwatt.elf -t bin -a 0x400000

After programming, you need to press the button labelled `PROG` on the Arty A7 (and probably the Nexys Video as well).

To connect to the FPGA, I usually use `picocom`:

    picocom -b 115200 -r -l /dev/ttyUSB1
